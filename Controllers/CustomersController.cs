﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Onlinestore.Models;

namespace Onlinestore.Controllers
{
    public class CustomersController : Controller
    {
        private OnlinestoreEntities db = new OnlinestoreEntities();

        // GET: Customers
        public ActionResult Index()
        {
            return View(db.Customers.ToList());
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Customer user)
        {
            var L1 = from usr in db.Customers orderby usr.customer_id select usr;

            if (ModelState.IsValid)
            {
                foreach (Customer userlist in L1)
                {
                    if (userlist.email == user.email && userlist.password == user.password)
                    {
                        Session["logged_in"] = 1;
                        Session["id"] = userlist.customer_id;
                        Session["fname"] = userlist.first_name;
                        Session["lname"] = userlist.last_name;
                        Session["email"] = userlist.email;
                        Session["phonenumber"] = userlist.phone_number;
                        Session["address"] = userlist.address;

                        return RedirectToAction("C_Index", "Account");
                    }

                }
                ModelState.AddModelError("incorrect", "Username or Password Incorrect!");
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            this.Session["logged_in"] = 0;
            Session["id"] = 0;
            Session["fname"] = null;
            Session["lname"] = null;
            Session["email"] = null;
            Session["phonenumber"] = null;
            Session["address"] = null;
            return RedirectToAction("Login", "Customers");
        }
        static bool isValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            try
            {
                // Use MailAddress class to validate email format
                var addr = new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public ActionResult Signup()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signup([Bind(Include = "customer_id,first_name,last_name,email,password,phone_number,address")] Customer user)
        {
            if (ModelState.IsValid)
            {
                var isEmailAlreadyExists = db.Customers.Any(x => x.email == user.email);
                if (!isValidEmail(user.email))
                    ModelState.AddModelError("email", "Invalid Email!");
                if (isEmailAlreadyExists)
                    ModelState.AddModelError("email", "Email already in use!");
                if (ModelState.IsValid)
                {
                    db.Customers.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Login");
                }
            }
            return View(user);
        }

        // GET: Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "customer_id,first_name,last_name,email,password,phone_number,address")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customer);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "customer_id,first_name,last_name,email,password,phone_number,address")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }



        // GET: Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Onlinestore.Models;

namespace Onlinestore.Controllers
{
    public class CartsController : Controller
    {
        private OnlinestoreEntities db = new OnlinestoreEntities();

        // GET: MyCart


        public ActionResult CartView()

        {
            List<Cart> cart = Session["Cart"] as List<Cart>;
            if (cart == null)

            {

                cart = new List<Cart>();

                Session["Cart"] = cart;

            }

            return View(cart);

        }

        public ActionResult PaidView()

        {

            return View();

        }
        // GET: Products/AddToCart

        [HttpPost]

        public ActionResult AddToCart(int id, decimal price, int quantity = 1)

        {

            List<Cart> cart = Session["Cart"] as List<Cart> ?? new List<Cart>();

            Cart existingItem = cart.FirstOrDefault(item => item.cart_id == id);

            if (existingItem != null)

            {

                existingItem.quantity += quantity;

            }

            else

            {

                cart.Add(new Cart { cart_id = id, price = Convert.ToInt32(price), quantity = quantity });

            }

            Session["Cart"] = cart;

            return RedirectToAction("CartView");

        }

        public ActionResult RemoveFromCart(int id)

        {

            List<Cart> cart = Session["Cart"] as List<Cart>;

            if (cart != null)

            {

                cart.RemoveAll(item => item.cart_id == id);

                Session["Cart"] = cart;

            }

            return RedirectToAction("CartView");

        }

        public int GetCurrentUserId()

        {
            return 1;

        }

        public ActionResult Checkout()

        {

            // Retrieve cart items from session

            List<Cart> cartItems = Session["Cart"] as List<Cart>;

            if (cartItems == null || cartItems.Count == 0)

            {

                // Redirect to an empty cart page or display a message

                return RedirectToAction("EmptyCart");

            }

            // Calculate total amount

            int price = (int)cartItems.Sum(item => (item.price * item.quantity));

            // Create a new order

            var buy = new Order

            {

                order_id = GetCurrentUserId(), // Replace with actual logic to get the current user ID

                total_price = price,

                order_date = DateTime.Now,

                // Set the initial order status

            };

            // Save the order to the database
            db.Orders.Add(buy);

            db.SaveChanges();

            // Optionally, you can clear the cart session after creating the order

            Session["Cart"] = null;

            // Redirect to the order confirmation page with the order ID

            return RedirectToAction("PaidView", new { orderId = buy.order_id });

        }
        public ActionResult Paid()
        {
            return View();
        }

    }

}



